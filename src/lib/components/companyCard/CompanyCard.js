import React from 'react';
import "./CompanyCard.scss";
import EditTwoToneIcon from '@material-ui/icons/EditTwoTone';

class CompanyCard extends React.Component {
  constructor(props) {
    super(props);
    this.onClick = this.handleClick.bind(this);
    this.handleChange = this.handleChange.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  };

  handleClick() {
    const form = document.getElementById('CompanyEdit');
    if (form.style.marginTop === '' || form.style.marginTop === '215px') {
      document.getElementById('CompanyShow').style.marginTop = '-215px';
      document.getElementById('CompanyEdit').style.marginTop = '0px';
    } else {
      document.getElementById('CompanyShow').style.marginTop = '0px';
      document.getElementById('CompanyEdit').style.marginTop = '215px';
    }
  }

  handleChange(event) {
    const target = event.target;
    const value = target.value;
    const name = target.name;
    this.setState({
      [name]: value
    })
  }

  handleSubmit() {
    const company = this.state;
    fetch(`https://food-and-drinks-api.herokuapp.com/companies/${this.state.id}`, {
      method: "PATCH",
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({company})
    })
    .then(response => response.json())
    .then(
      updateView()
    )
    .catch(
      (error) => {
        this.setState({
          isLoaded: false,
          error
        });
      }
    )
  }

  render() {
    const { name, description, address, zip, city } = this.props.company.companies;
    if (name) {
      return (
        <div className="CompanyCard">
        <div id="CompanyShow">
        <div className="CompanyCardHeader">
        <h2>{name}</h2>
        <div className="Circle">
        <EditTwoToneIcon onClick={this.handleClick} />
        </div>
        </div>
        <p className="Description">{description}</p>
        <p>{address}</p>
        <p>{zip} {city}</p>
        </div>
        <div id="CompanyEdit">

        </div>
        </div>
      )
    } else {
      return null
    }
  }
}

function updateView() {
  const show = document.getElementById('CompanyShow');
  const edit = document.getElementById('CompanyEdit');
  if (show) {
    show.style.marginTop = '0px';
  }
  if (edit) {
    edit.style.marginTop = '0px';
  }
  // document.getElementById('CompanyShow').style.marginTop = '0px';
  // document.getElementById('CompanyEdit').style.marginTop = '215px';
}

export default CompanyCard;
