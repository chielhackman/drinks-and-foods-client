import React from 'react';
import "./Search.scss";

class Search extends React.Component {
  render() {
    return (
      <div className="Search">
        <input type="text" onChange={this.props.handleChange} placeholder="Search a bar or restaurant" />
      </div>
    )
  }
}

export default Search;
