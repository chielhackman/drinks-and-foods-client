import React from 'react';
import '../styles/App.scss';
import Search from './search/Search';
import Companies from './companies/Companies';
import CompanyCard from './companyCard/CompanyCard';

class App extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      error: null,
      isLoaded: false,
      companies: [],
      selectedCompany: null
    }
    this.handleChange = this.handleChange.bind(this);
  }

  componentDidMount() {
    fetch("https://food-and-drinks-api.herokuapp.com/companies", {
      method: "GET"
    })
    .then(response => response.json())
    .then(
      (data) => this.setState({
        isLoaded: true,
        companies: data,
        selectedCompany: data[0]
      })
    )
    .catch(
      (error) => {
        this.setState({
          isLoaded: false,
          error
        });
      }
    )
  }

  handleChange(event) {
    fetch(`https://food-and-drinks-api.herokuapp.com/companies?search=${event.target.value}`, {
      method: "GET"
    })
    .then(response => response.json())
    .then(
      (data) => this.setState({
        isLoaded: true,
        companies: data
      })
    )
    .catch(
      (error) => {
        this.setState({
          isLoaded: false,
          error
        });
      }
    )
  }

  render() {
    return (
      <div className="App">
        <Search handleChange={this.handleChange} />
        <div className="Companies">
          <div className="CompaniesHeader">
            <h1>Companies</h1>
            <button type="button" className="BtnNewCompany">New company</button>
          </div>
          <div className="CompaniesContent">
            <Companies companies={this.state} />
            <CompanyCard company={this.state} />
          </div>
        </div>
      </div>
    );
  }
}

export default App;
