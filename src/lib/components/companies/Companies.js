import React from 'react';
import Company from '../company/Company';
import './Companies.scss';

class Companies extends React.Component {
  handleClick(event) {
    fetch(`https://food-and-drinks-api.herokuapp.com/companies/${event}`, {
      method: "GET"
    })
    .then(response => response.json())
    .then(
      (data) => this.setState({
        selectedCompany: data
      })
    )
    .catch(
      (error) => {
        this.setState({
          isLoaded: false,
          error
        });
      }
    )
  }

  render() {
    const { error, isLoaded, companies } = this.props.companies;
    if (error) {
      return <div>Error: {error.message}</div>;
    } else if (!isLoaded) {
      return <div>Loading...</div>;
    } else {
      return (
        <div className="CompaniesList">
          {companies.map(company => (
            <Company key={company.id} company={company} onClick={this.handleClick.bind(this, company.id)} />
          ))}
        </div>
      )
    }
  };
}

export default Companies;
