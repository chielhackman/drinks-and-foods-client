import React from 'react';
import "./Company.scss";

class Company extends React.Component {
  render() {
    console.log(this.props.company.id)
    console.log(this.props.selectedCompany)
    return (
      <div className={activeClass(this.props.company.id, this.props.selectedCompany)} onClick={this.props.onClick}>
        <h2>{this.props.company.name}</h2>
        <p>{this.props.company.description}</p>
      </div>
    )
  };
}

function activeClass(companyId, selectedCompany) {
  if (selectedCompany && companyId === selectedCompany.id) {
    return 'Company Active';
  } else {
    return 'Company';
  }
}

export default Company;
